﻿using Google.Authenticator;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Security.Permissions;
using twoFactors.Core;
using twoFactors.Models;

namespace twoFactors.Controllers
{
    public class LoginController : Controller
    {
        private const string key = "Dangerous.2023";
        const string FolderName = "products";
        public LoginController() { }

        public IActionResult Index()
        {
            string isValidAuthentication = HttpContext.Session.GetString("IsValidAuthentication");
            if (!string.IsNullOrEmpty(isValidAuthentication) && isValidAuthentication == true.ToString()) return RedirectToAction("Index", "Home");
            return View();
        }

        [HttpPost]
        [ActionName("Index")]
        public IActionResult Login(UserViewModel user)
        {
            string message;
            bool status = false;

            MongoClient dbClient = new MongoClient("mongodb+srv://joseph:tDndLmO7zNva5wbi@cluster0.n4xlzkh.mongodb.net/test");
            var database = dbClient.GetDatabase("upsSecurity");
            var collection = database.GetCollection<BsonDocument>("upsUsers");
            var filter = Builders<BsonDocument>.Filter.Eq("username", user.Username) & Builders<BsonDocument>.Filter.Eq("password", user.Password);
            var count = collection.CountDocuments(filter);
            if (count > 0) 
            {
                status = true; //It indicates 2FA form
                message = "2FA Verification";
                HttpContext.Session.SetString("userName", user.Username);

                string uniqueKeyforUser = (user.Username + key);
                HttpContext.Session.SetString("userUniqueKey", uniqueKeyforUser);

                
                
                //2FA Setup
                TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                var setupInfo = tfa.GenerateSetupCode("upsJoseCastilloApp", user.Username, uniqueKeyforUser, false);




                ViewBag.BarcodeImageUrl = setupInfo.QrCodeSetupImageUrl;
                ViewBag.SetupCode = setupInfo.ManualEntryKey;
            }
            else message = "Invalid credential";

            ViewBag.Message = message;
            ViewBag.Status = status;
            return View();
        }

        public IActionResult Logout(UserViewModel user)
        {
            ViewBag.Status = false;
            HttpContext.Session.SetString("IsValidAuthentication", false.ToString());
            return RedirectToAction("Index");
        }

        

        public HttpContext GetHttpContext()
        {
            return HttpContext;
        }

        public IActionResult Verify2FA(string passcode)
        {

            var token = passcode;// HttpContext.Request.

            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();

            string UserUniqueKey = HttpContext.Session.GetString("userUniqueKey");

            bool isValid = tfa.ValidateTwoFactorPIN(UserUniqueKey, token);

            if (isValid)
            {
                HttpContext.Session.SetString("IsValidAuthentication", true.ToString());
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index", "Login");
        }

        public IActionResult SignUp()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(RegisterViewModel user)
        {
            if (!ModelState.IsValid) return View("SignUp", user);

            MongoClient dbClient = new MongoClient("mongodb+srv://joseph:tDndLmO7zNva5wbi@cluster0.n4xlzkh.mongodb.net/test");
            var database = dbClient.GetDatabase("upsSecurity");
            var document = new BsonDocument { { "username", user.Username }, { "password", user.Password }};
            var collection = database.GetCollection<BsonDocument>("upsUsers");
            collection.InsertOne(document);

            return RedirectToAction("Index");
        }
    }
}
