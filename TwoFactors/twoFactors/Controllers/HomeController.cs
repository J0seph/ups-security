﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using twoFactors.Models;

namespace twoFactors.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            string isValidAuthentication = HttpContext.Session.GetString("IsValidAuthentication");
            if (string.IsNullOrEmpty(isValidAuthentication) || isValidAuthentication == false.ToString()) return RedirectToAction("Index", "Login");
            return View(true);
        }

        public IActionResult Privacy()
        {
            string isValidAuthentication = HttpContext.Session.GetString("IsValidAuthentication");
            if (string.IsNullOrEmpty(isValidAuthentication) || isValidAuthentication == false.ToString()) return RedirectToAction("Index", "Login");
            return View(true);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}