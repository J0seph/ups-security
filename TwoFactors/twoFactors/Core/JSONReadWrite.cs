﻿namespace twoFactors.Core
{
    public class JSONReadWrite
    {
        public JSONReadWrite() { }

        public string Read(string fileName, string location)
        {
            string path = Path.Combine(
                    Directory.GetCurrentDirectory(),
                    "App_Data",
                    location,
                fileName);

            string jsonResult;
            jsonResult = File.ReadAllText(path);
            return jsonResult;
        }

        public void Write(string fileName, string location, string jSONString)
        {
            string path = Path.Combine(
                    Directory.GetCurrentDirectory(),
                    "App_Data",
                    location,
                    fileName);
            File.WriteAllText(path, jSONString);
        }
    }
}
